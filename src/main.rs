use clap::{App, Arg};

use mosaic_generator::*;

fn main() -> Result<()> {
    stable_eyre::install()?;

    let matches = App::new("Photomosaic Generator")
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about("Creates a photomosaic from an input file and source images")
        .arg(
            Arg::with_name("input")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("output")
                .help("Sets the location of the created image")
                .short("o")
                .long("output")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("source")
                .help("Sets the directory of source images")
                .short("s")
                .long("source")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("tile size")
                .help("Sets the size of composing thumbnails in pixels")
                .short("t")
                .long("tile_size")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("generate")
                .help("Generates new cropped source images")
                .short("g")
                .long("generate"),
        )
        .get_matches();

    // put cli args into variables
    let input_path = matches.value_of("input").unwrap();
    let output_path = matches.value_of("output").unwrap();
    let cropped_path = match cfg!(windows) {
        true => "cropped_sources\\",
        false => "./cropped_sources/",
    };
    let source_dir = matches.value_of("source").unwrap();
    let generate = matches.is_present("generate");
    let tile_size = matches.value_of("tile size");

    run(
        input_path,
        output_path,
        cropped_path,
        source_dir,
        generate,
        tile_size,
    )?;

    Ok(())
}
