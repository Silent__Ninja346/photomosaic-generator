#[derive(Debug)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl Color {
    pub fn diff(&self, other: &Color) -> f64 {
        let r_diff = (self.red as f64 - other.red as f64).powi(2);
        let g_diff = (self.green as f64 - other.green as f64).powi(2);
        let b_diff = (self.blue as f64 - other.blue as f64).powi(2);
        (r_diff + g_diff + b_diff).sqrt()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn color_difference() {
        let c1 = Color {
            red: (100),
            green: (100),
            blue: (100),
        };
        let c2 = Color {
            red: (0),
            green: (0),
            blue: (0),
        };
        let difference1 = c1.diff(&c2).floor() as u8;
        let difference2 = c2.diff(&c1).floor() as u8;
        assert_eq!(difference1, 173);
        assert_eq!(difference2, 173);
    }
}
